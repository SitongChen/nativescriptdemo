import MainPage from './views/MainPage';
import './app.scss'

const Vue = require('nativescript-vue/dist/index')
const http = require("http")

Vue.prototype.$http = http

new Vue({
  components: {
    MainPage,
  },

  template: `
    <main-page></main-page>
  `,
  methods: {
  }
}).$start()
