module.exports =
webpackJsonp([0],{

/***/ 116:
/* no static exports found */
/*!******************!*\
  !*** ./app.scss ***!
  \******************/
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 119:
/* exports provided: default */
/* exports used: default */
/*!****************************!*\
  !*** ./views/MainPage.vue ***!
  \****************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_ns_vue_loader_lib_selector_type_script_index_0_bustCache_MainPage_vue__ = __webpack_require__(/*! !babel-loader!../../~/ns-vue-loader/lib/selector?type=script&index=0&bustCache!./MainPage.vue */ 127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_ns_vue_loader_lib_template_compiler_index_id_data_v_11b9df17_hasScoped_false_buble_transforms_node_modules_ns_vue_loader_lib_selector_type_template_index_0_bustCache_MainPage_vue__ = __webpack_require__(/*! !../../~/ns-vue-loader/lib/template-compiler/index?{"id":"data-v-11b9df17","hasScoped":false,"buble":{"transforms":{}}}!../../~/ns-vue-loader/lib/selector?type=template&index=0&bustCache!./MainPage.vue */ 133);
var disposed = false
var normalizeComponent = __webpack_require__(/*! ../../~/ns-vue-loader/lib/component-normalizer */ 19)
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_ns_vue_loader_lib_selector_type_script_index_0_bustCache_MainPage_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_ns_vue_loader_lib_template_compiler_index_id_data_v_11b9df17_hasScoped_false_buble_transforms_node_modules_ns_vue_loader_lib_selector_type_template_index_0_bustCache_MainPage_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "views/MainPage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("ns-vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-11b9df17", Component.options)
  } else {
    hotAPI.reload("data-v-11b9df17", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 122:
/* no static exports found */
/* all exports used */
/*!****************!*\
  !*** ./app.js ***!
  \****************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__views_MainPage__ = __webpack_require__(/*! ./views/MainPage */ 119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_scss__ = __webpack_require__(/*! ./app.scss */ 116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__app_scss__);



var Vue = __webpack_require__(/*! nativescript-vue/dist/index */ 27);
var http = __webpack_require__(/*! http */ 26);

Vue.prototype.$http = http;

new Vue({
  components: {
    MainPage: __WEBPACK_IMPORTED_MODULE_0__views_MainPage__["a" /* default */]
  },

  template: '\n    <main-page></main-page>\n  ',
  methods: {}
}).$start();

/***/ }),

/***/ 126:
/* exports provided: default */
/* exports used: default */
/*!******************************************************************************************************************!*\
  !*** ../~/babel-loader/lib!../~/ns-vue-loader/lib/selector.js?type=script&index=0&bustCache!./views/AddPage.vue ***!
  \******************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {};
  },
  methods: {
    onTap: function onTap() {
      alert("Good Luck");
      this.$navigateBack();
    }
  }
});

/***/ }),

/***/ 127:
/* exports provided: default */
/* exports used: default */
/*!*******************************************************************************************************************!*\
  !*** ../~/babel-loader/lib!../~/ns-vue-loader/lib/selector.js?type=script&index=0&bustCache!./views/MainPage.vue ***!
  \*******************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__AddPage__ = __webpack_require__(/*! ./AddPage */ 131);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
  components: {
    AddPage: __WEBPACK_IMPORTED_MODULE_0__AddPage__["a" /* default */]
  },
  data: function data() {
    return {
      greet: "Lol",
      page: __WEBPACK_IMPORTED_MODULE_0__AddPage__["a" /* default */],
      listData: [{
        "key": "598a678278fee204ee51cd2c",
        "name": "Cream Tea",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/b/bf/Cornish_cream_tea_2.jpg",
        "foodDescription": "This is a cup of cream tea"
      }, {
        "key": "598a684f78fee204ee51cd2f",
        "name": "Fresh mushroom",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/6/6e/Lactarius_indigo_48568.jpg",
        "foodDescription": "Fresh mushroom with vegetables. This is a long line, this is a long line, this is a long line,this is a long line,this is a long line"
      }, {
        "key": "598a687678fee204ee51cd30",
        "name": "Japanese Oyster",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/d/d2/Oysters_served_on_ice%2C_with_lemon_and_parsley.jpg",
        "foodDescription": "Oysters with ice rock"
      }, {
        "key": "598a680178fee204ee51cd2e",
        "name": "Korean Kimchi",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/7/74/Yeolmukimchi_3.jpg",
        "foodDescription": "Traditional Korean Food"
      }, {
        "key": "598a688878fee204ee51cd31",
        "name": "Multiple salad",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/9/94/Salad_platter.jpg",
        "foodDescription": "Salad mixed with mushroom"
      }, {
        "key": "598a68b778fee204ee51cd32",
        "name": "Vegetable",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/6/6c/Vegetable_Cart_in_Guntur.jpg",
        "foodDescription": "Fresh vegetables"
      }, {
        "key": "598a67c478fee204ee51cd2d",
        "name": "traditional japanese salad",
        "imageUrl": "https://upload.wikimedia.org/wikipedia/commons/a/ac/Simple_somen.jpg",
        "foodDescription": "Very delicious Japanese Salad"
      }]
    };
  },
  methods: {
    onTap: function onTap() {
      alert("Good Luck");
      console.log(this);
    }
  }
});

/***/ }),

/***/ 131:
/* exports provided: default */
/* exports used: default */
/*!***************************!*\
  !*** ./views/AddPage.vue ***!
  \***************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_ns_vue_loader_lib_selector_type_script_index_0_bustCache_AddPage_vue__ = __webpack_require__(/*! !babel-loader!../../~/ns-vue-loader/lib/selector?type=script&index=0&bustCache!./AddPage.vue */ 126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_ns_vue_loader_lib_template_compiler_index_id_data_v_6ed3e9f1_hasScoped_false_buble_transforms_node_modules_ns_vue_loader_lib_selector_type_template_index_0_bustCache_AddPage_vue__ = __webpack_require__(/*! !../../~/ns-vue-loader/lib/template-compiler/index?{"id":"data-v-6ed3e9f1","hasScoped":false,"buble":{"transforms":{}}}!../../~/ns-vue-loader/lib/selector?type=template&index=0&bustCache!./AddPage.vue */ 135);
var disposed = false
var normalizeComponent = __webpack_require__(/*! ../../~/ns-vue-loader/lib/component-normalizer */ 19)
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_ns_vue_loader_lib_selector_type_script_index_0_bustCache_AddPage_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_ns_vue_loader_lib_template_compiler_index_id_data_v_6ed3e9f1_hasScoped_false_buble_transforms_node_modules_ns_vue_loader_lib_selector_type_template_index_0_bustCache_AddPage_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "views/AddPage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("ns-vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6ed3e9f1", Component.options)
  } else {
    hotAPI.reload("data-v-6ed3e9f1", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 133:
/* exports provided: default */
/* exports used: default */
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ../~/ns-vue-loader/lib/template-compiler?{"id":"data-v-11b9df17","hasScoped":false,"buble":{"transforms":{}}}!../~/ns-vue-loader/lib/selector.js?type=template&index=0&bustCache!./views/MainPage.vue ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var esExports = { template: '  \n  <Page>\n    <ActionBar title=\"Main Page\" >\n      <ActionItem text=\"ADD\" ios.position=\"right\"  @tap=\"$navigateTo(page)\"/>\n    </ActionBar>\n    <ListView height=\"600\" for=\"item in listData\" @itemTap=\"onTap\">\n      <v-template>\n      <FlexboxLayout alignItems=\"flex-start\">\n        <Image borderWidth=\"10\" borderColor=\"#ffffff\" height=\"100\" width=\"100\" :src=\"item.imageUrl\"  stretch=\"aspectFit\"/>\n        <FlexboxLayout flexDirection=\"column\">\n          <Label borderTopWidth=\"10\" borderColor=\"#ffffff\"  :text=\"item.name\"  />\n          <Label width=\"275\" textWrap=\"true\" :text=\"item.foodDescription\"  />\n        </FlexboxLayout>\n      </FlexboxLayout>\n      </v-template>\n    </ListView>\n  \n  </Page>\n  ' }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ 135:
/* exports provided: default */
/* exports used: default */
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ../~/ns-vue-loader/lib/template-compiler?{"id":"data-v-6ed3e9f1","hasScoped":false,"buble":{"transforms":{}}}!../~/ns-vue-loader/lib/selector.js?type=template&index=0&bustCache!./views/AddPage.vue ***!
  \************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var esExports = { template: '  \n  <Page>\n    <ActionBar title=\"Add New Item\" />\n    <ScrollView orientation=\"vertical\">\n      <StackLayout >\n        <Image src=\"https://upload.wikimedia.org/wikipedia/commons/b/bf/Cornish_cream_tea_2.jpg\" />\n        <StackLayout orientation=\"horizontal\" >\n          <Label text=\"Name\" height=\"70\" width=\"100\" backgroundColor=\"#43b883\"/>\n          <TextField width=\"100%\" :text=\"textFieldValue\" hint=\"Enter text...\" />\n        </StackLayout>\n        <StackLayout orientation=\"horizontal\" >\n          <Label text=\"Description\" height=\"70\" width=\"100\" backgroundColor=\"#289062\"/>\n          <TextField width=\"100%\" :text=\"textFieldValue\" hint=\"Enter text...\" />\n        </StackLayout>\n        <Button width=\"120\" height=\"60\" borderRadius=\"15\" borderColor=\"#50BBFF\" borderWidth=\"2\" text=\"SUBMIT\" @tap=\"onTap\"/>\n      </StackLayout>\n    </ScrollView>\n  </Page>\n  ' }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),

/***/ 19:
/* no static exports found */
/* all exports used */
/*!******************************************************!*\
  !*** ../~/ns-vue-loader/lib/component-normalizer.js ***!
  \******************************************************/
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.template = compiledTemplate.template
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ })

},[122]);